{ config, home, lib, pkgs, ... }:
let
  fpm-config = pkgs.writeText "php-fpm.conf" ''
    [global]
    error_log = ${home}/error.log
    [www]
    catch_workers_output = yes
    listen = ${toString config.port}
    pm = ondemand
    pm.max_children = 10
    request_slowlog_timeout = 5s
    slowlog = ${home}/slow.log
  '';
  php-ini = pkgs.writeText "php.ini" ''
    curl.cainfo = ${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt
    date.timezone = UTC
  '';
in {
  exports = {
    inherit (config) port;
  };
  options.port = lib.mkOption {
    type = lib.types.int;
    default = 9000;
  };
  run = ''
    ${pkgs.runit}/bin/chpst -u www-data \
      ${pkgs.php}/bin/php-fpm \
        --nodaemonize \
        --fpm-config ${fpm-config} \
        --php-ini ${php-ini}
  '';
}
